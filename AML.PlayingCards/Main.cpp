#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum RANK {
	ACE = 14,
	KING = 13,
	QUEEN = 12,
	JACK = 11,
	TEN = 10,
	NINE = 9,
	EIGHT = 8,
	SEVEN = 7,
	SIX = 6,
	FIVE = 5,
	FOUR = 4,
	THREE = 3,
	TWO = 2

};

enum SUIT {
	CLUBS,
	HEARTS,
	SPADES,
	DIAMONDS
};

struct Card {
	SUIT suit;
	RANK rank;
};

void PrintCard(Card Card) {

	cout << "The ";

	switch (Card.rank)
	{
	case 14: cout << "Ace";
		break;
	case 13: cout << "King";
		break;
	case 12: cout << "Queen";
		break;
	case 11: cout << "Jack";
		break;
	case 10: cout << "Ten";
		break;
	case 9: cout << "Nine";
		break;
	case 8: cout << "Eight";
		break;
	case 7: cout << "Seven";
		break;
	case 6: cout << "Six";
		break;
	case 5: cout << "Five";
		break;
	case 4: cout << "Four";
		break;
	case 3: cout << "Three";
		break;
	case 2: cout << "Two";
		break;
	}

	cout << " of ";

	switch (Card.suit)
	{
	case CLUBS: cout << "Clubs";
		break;
	case HEARTS: cout << "Hearts";
		break;
	case SPADES: cout << "Spades";
		break;
	case DIAMONDS: cout << "Diamonds";
		break;
	}

	cout << "\n";
};

Card HighCard(Card card1, Card card2) {
	if (card1.rank > card2.rank) {
		cout << "Card One has a higher rank than Card Two" << "\n";
		return card1;
	}
	else {
		cout << "Card Two has a higher rank than Card One";
		return card2;
	}
};

int main(){

	Card card1;
	Card card2;

	card1.rank = TWO;
	card1.suit = HEARTS;

	card2.rank = SEVEN;
	card2.suit = SPADES;

	PrintCard(card1);
	PrintCard(card2);

	HighCard(card1, card2);


	_getch();
	return 0;
}